#include <thread>
#include <vector>
#include <atomic>
#include <chrono>

#include <gtest/gtest.h>
#include <semaphore.h>

void run(int threads_count, int concurrency_level) {
    Semaphore semaphore(concurrency_level);
    for (int i = 0; i < concurrency_level; ++i)
        semaphore.enter();

    int time = 0;
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    std::atomic<bool> is_failed(false);
    for (int i = 0; i < threads_count; ++i) {
        threads.emplace_back([&time, &semaphore, &is_failed, i]() {
            semaphore.enter([&time, &is_failed, i]() {
                auto cur_time = time++;
                if (i != cur_time)
                    is_failed = true;
            });
            semaphore.leave();
        });
        std::this_thread::sleep_for(std::chrono::duration<int, std::milli> (300));
    }

    for (int i = 0; i < concurrency_level; ++i) {
        semaphore.leave();
    }

    for (int i = 0; i < threads_count; ++i) {
        threads[i].join();
    }

    ASSERT_FALSE(is_failed);
}

TEST(Order, Mutex) {
    run(6, 1);
}

TEST(Order, Semaphore) {
    run(6, 3);
}

TEST(Order, Advanced) {
    std::vector<std::thread> threads;
    threads.reserve(3);
    Semaphore semaphore(3);
    std::atomic<bool> is_failed(false);
    for (int i = 0; i < 3; ++i)
        semaphore.enter();

    int time = 0;

    for (int i = 0; i < 3; ++i) {
        threads.emplace_back([&semaphore, &is_failed, &time, i]() {
            semaphore.enter([&semaphore, &is_failed, &time, i]() {
                int cur_time = time++;
                if (i != cur_time)
                    is_failed.store(true);
                if (i < 2)
                    std::this_thread::sleep_for(std::chrono::duration<int, std::milli> (500));
            });
            semaphore.leave();
        });
        std::this_thread::sleep_for(std::chrono::duration<int, std::milli> (300));
    }

    for (int i = 0; i < 3; ++i)
        semaphore.leave();

    for (auto& cur : threads)
        cur.join();

    ASSERT_FALSE(is_failed);
}
