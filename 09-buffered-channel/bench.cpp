#include <benchmark/benchmark.h>
#include <benchmark/benchmark_api.h>
#include <buffered_channel.h>

#include <thread>
#include <atomic>

void send(int count, BufferedChannel<int>& channel) {
    auto func = [count, &channel](int start) {
        for (int i = start; i < count; i += 2)
            channel.send(i);
    };
    std::thread even(func, 0);
    std::thread odd(func, 1);
    even.join();
    odd.join();
    channel.close();
}

int64_t calc_sum(int count, int buff_size) {
    BufferedChannel<int> channel(buff_size);
    std::thread sender_thread(send, count, std::ref(channel));
    int threads_count = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    std::atomic<int64_t> sum(0);
    for (int i = 0; i < threads_count; ++i)
        threads.emplace_back([&channel, &sum]() {
            for (;;) {
                auto value = channel.recv();
                if (!value.second)
                    break;
                sum += value.first;
            }
        });

    for (auto& cur : threads)
        cur.join();

    sender_thread.join();

    return sum;
}

void run(benchmark::State& state) {
    int count = 1e6;
    int64_t ok_ans = static_cast<int64_t> (count) * (count - 1) / 2;
    while (state.KeepRunning()) {
        int buff_size = state.range(0);
        if (ok_ans != calc_sum(count, buff_size))
            state.SkipWithError("Wrong sum");
    }
}

BENCHMARK(run)->Arg(10)->Arg(100)->Arg(10000)->UseRealTime()->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
