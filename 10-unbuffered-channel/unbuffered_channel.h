#pragma once

#include <utility>

template<class T>
class UnbufferedChannel {
public:
    void send(const T& value);
    std::pair<T, bool> recv();
    void close();
};
