# Что нужно доделать к первому семинару в системе тестирования (порядок случайный)
  - [x] Скрипт создающий репозитории студентов по заявке
  - [ ] Проверка freeze-файла на тестирующем runner-е
  - [ ] Проверить руками все права
  - [ ] Написать README.md, как сдавать задачи, как настраивать окружение, как запускать cmake
  - [ ] Настроить тестирующий runner & тестирующий код
  - [ ] Настроить clang-format и clang-tidy проверки перед build-ом. Описать как пользоваться локально.
  - [ ] Доделать course_sheet.py, интегрировать в тестовый runner
  - [ ] Сделать сравнение benchmark-а c baseline-ом
  - [ ] Написать инструкцию по virtualbox для Windows и MacOS пользователей
  - [ ] Поддержать ubuntu 16.10 для студентов которые на ней
  - [ ] Настроить деплой (пуш в master ветки студенческих репозиториев)
  - [ ] Настроить concurrency на runner-е

# Что можно сделать потом
  - [ ] Синхронизировать CONTRIBUTING.md с реальностью
  - [ ] Измерять потребление памяти в бенчмарках
  - [ ] Добавить настройку `review: True|False`
