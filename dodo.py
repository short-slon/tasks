import yaml
import os
import glob

from doit.tools import result_dep


TASK_LIST=[
    "01-hello-world",
    "02-is-prime",
    "03-reduce",
    "04-hash-table",
    "05-meet-in-the-middle",
    "06-psnappy",
]

DOIT_CONFIG = {
#    'default_tasks': []
}

def gen_cmake(name, build_type, solution=None):
    task_name = "cmake:{}:{}".format(name, build_type)
    build_dir = os.path.join(name, "build", build_type)
    cmake_cmd = "cmake ../../ -G Ninja -DCMAKE_BUILD_TYPE={}".format(build_type)
    
    if solution is not None:
        task_name += "_" + solution
        build_dir += "_" + solution
        cmake_cmd += " -DTEST_SOLUTION=" + solution
        cmake_cmd += " -DENABLE_PRIVATE_TESTS=1"
        
    return {
        "basename": task_name,
        "targets": [os.path.join(build_dir, "build.ninja")],
        "file_dep": ["dodo.py"],
        "actions": [
            "mkdir -p " + build_dir,
            "cd {} && {}".format(build_dir, cmake_cmd)
        ]
    }


def run_build(name, build_type, config, solution=None):
    task_name = "ninja:{}:{}".format(name, build_type)
    build_dir = os.path.join(name, "build", build_type)

    binaries = config.get("gtests", []) + config.get("benchmarks", [])
    
    if solution is not None:
        task_name += "_" + solution
        build_dir += "_" + solution

    return {
        "basename": task_name,
        "targets": list({os.path.join(build_dir, b.split("/")[1])
                         for b in binaries}),
        "file_dep": [os.path.join(build_dir, "build.ninja")],
        "actions": ["ninja -v -C " + build_dir],
        "uptodate": [False]
    }


def run_gtest(name, binary_name, mustfail=False):
    task_name = "gtest:{}:{}".format(name, binary_name)
    builds_dir = os.path.join(name, "build")

    compilation_task = "ninja:{}:{}".format(name, binary_name.split("/")[0])

    if not mustfail:
        run_cmd = "./{}/{}".format(builds_dir, binary_name)
    else:
        run_cmd = "!(./{}/{})".format(builds_dir, binary_name)
    
    return {
        "basename": task_name,
        "uptodate": [False],
        "actions": [
            run_cmd
        ]
    }
    

def test_single(name):
    config = yaml.load(open(os.path.join(name, ".tester-config.yaml")))

    for build_type in config["builds"]:
        yield gen_cmake(name, build_type)
        yield run_build(name, build_type, config)

        for solution in config.get("solutions", []):
            yield gen_cmake(name, build_type, solution)
            yield run_build(name, build_type, config, solution)

    # ensure that some solutions pass
    for binary_name in config.get("check_ok_gtests", []):
        yield run_gtest(name, binary_name, mustfail=False)

    # ensure that some solutions fail
    for binary_name in config.get("check_fail_gtests", []):
        yield run_gtest(name, binary_name, mustfail=True)


def task_test_all():
    for task in TASK_LIST:
        yield test_single(task)

def disabled_task_clang_format():
    for task in TASK_LIST:
        files = glob.glob(task + "/*.cpp")
        files += glob.glob(task + "/*.h")
        files += glob.glob("./private/" + task[3:] + "/*.cpp")
        files += glob.glob("./private/" + task[3:] + "/*.h")

        yield {
            'name': task,
            'actions': ['clang-format -i -style=Google ' + ' '.join(files)]
        }
